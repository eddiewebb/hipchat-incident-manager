var IncidentRoom = function(roomId,issueKey,summary){

    this.roomId = roomId;
    this.issueKey = issueKey;
    this.summary = summary;


    this.incidentHistory = [];


    this.updateStatus = function(status,message){
        this.incidentHistory.push({"status":status,"message":message,"date":new Date()});
    };

    this.getLatestStatus = function(roomId){
    return this.incidentHistory[this.incidentHistory.length-1];
    };
    this.getLatestStatusText = function(roomId){
        var last = this.getLatestStatus(roomId);
        return '<strong>' + last.status + '</strong> ' + last.message;
    };
    this.printHistory = function(roomId){
        var result="<strong>History for " + this.issueKey + " (reverse order)</strong><br><ul>";
        for (var i = this.incidentHistory.length - 1; i >= 0; i--) {
            result+= '<li>'+this.incidentHistory[i].date +': '+ this.incidentHistory[i].status +', ' + this.incidentHistory[i].message +'</li>';
        }
        result+="</ul>";
        return result;
    };

	this.reportColor = function(){
		current = this.getLatestStatus();
		console.log("Latest",current);
		if(current.status === "OPEN") {
			return "yellow";
		}else if(current.status === "ESCALATED") {
			return "red";
		}else if(current.status === "RESOLVED") {
			return "green";
		}else {
			return "purple";
		}
	};

    //still constructor, js is funny like that..

    this.updateStatus("OPEN",summary);

};

exports.IncidentRoom = IncidentRoom;
