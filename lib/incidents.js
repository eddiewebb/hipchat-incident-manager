var IncidentRoom = require('./incidentRoom').IncidentRoom;

// Pattern in common rooms to spawn or get help
var commonRoomPattern = new RegExp(/^(\/[a-z]*)\s(OPENHERE|OPEN|ESCALATE|RESOLVED|STATUS|ACK)?\s?([A-Z]{2,16}-[0-9]*)?\s?(.*)$/);
var getHelpPattern = new RegExp(/^\/incident help$/);
// Pattern for active rooms
var pagePattern = new RegExp(/^\/page\s((\S*)\s?)*$/);


var incidentRooms = new Array([]);


/*
* Internal methods
*/

function getLatestStatusText(roomId){
  return incidentRooms[roomId].getLatestStatusText();
}


function getLatestStatus(roomId){
  return incidentRooms[roomId].getLatestStatus();
}






/*
* Methods exposed to routes
*/


module.exports = function(addon,app,hipchat) {

    return{

    report:function(req,res){

        var roomId = req.context.item.room.id;
        if(incidentRooms[roomId] ){
          ir = incidentRooms[roomId];
			
         hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Current Incident Status '  + ir.getLatestStatusText() + '<br>' + ir.printHistory() ,{"options":{"color":ir.reportColor()}})
            .then(function(data){
              res.send(200);
            });
        }else{
            hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'No active incidents!',{"options":{"color":"gray"}} )
            .then(function(data){
              res.send(200);
            });
        }
    },


     updateIncident:function(req,res){
            //not looking for a report
            var messageText = req.context.item.message.message;
            var roomId = req.context.item.room.id;
            var arrMatches = messageText.match(commonRoomPattern);
            if(arrMatches){
				var slash = arrMatches[1];
				var qualifier = arrMatches[2];
				var issueKey = arrMatches[3];
				var message = arrMatches[4];

				console.log("Message:",messageText);
                if(slash === '/new' || (slash === "/incident" && qualifier === "OPEN")){
                    //console.log(req.context.item);
                    if( issueKey === null ){
                      hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'I\'ll need a valid JIRA key to open an incident.\n Type "/incident help" for assitance',{"options":{"color":"gray"}})
                        .then(function(data){
                          res.send(200);
                      });
                    }else{
                      hipchat.createRoom(req.clientInfo,issueKey,req.context.item.message.from.id)
                      .then(function(data){
                        if (data.statusCode === 201){
                          newRoomId=data.body.id;
                          newInc = new IncidentRoom(newRoomId,issueKey,message);
                          incidentRooms[newRoomId]=newInc;
                          requestorId=req.context.item.message.from.id;
                          hipchat.sendMessage(req.clientInfo, roomId, 'Incident Room Created: '+issueKey )
                            .then(function(data){
                              hipchat.sendMessage(req.clientInfo,newRoomId,'Welcome @' + req.context.item.message.from.mention_name + ', please join this room to invite others.',{"options":{"format":"text","notify":true}})
                              .then(function(data){res.send(200);});
                            });
                        }else{
                          console.log(data);
                            hipchat.sendMessage(req.clientInfo, roomId, 'Failed to create room: ' + data.body.error.message )
                            .then(function(data){
                            res.send(200);
                            });
                        }
                      });
                    }

                    
                }else if(slash === "/incident" && qualifier === "OPENHERE"){
                  newInc = new IncidentRoom(req.context.item.room.id,issueKey,message);
                          incidentRooms[req.context.item.room.id]=newInc;
                    
                    hipchat.sendMessage(req.clientInfo, roomId, 'Incident Opened: ' + getLatestStatus(roomId).message )
                        .then(function(data){
                        res.send(200);
                        });
                }else if(slash === '/acknowledge' || (slash === "/incident" && qualifier === 'ACK')){
                    incident = incidentRooms[req.context.item.room.id];
					user = req.context.item.message.from.mention_name;
                  	incident.updateStatus("ACKNOWLEDGED",'Acknowledged by @' + user + '>' + message?message:getLatestStatus(roomId).message);
                    
                    hipchat.sendMessage(req.clientInfo, roomId, 'Acknowledged by @' + user,{"options":{"format":"text"}} )
                        .then(function(data){
                        res.send(200);
                        });
                }else if(slash === '/escalate' || (slash === "/incident" && qualifier === "ESCALATE")){
                    incident = incidentRooms[req.context.item.room.id];
                    if ( incident !== null ){
                       incident.updateStatus("ESCALATED",message?message:getLatestStatus(roomId).message);
                        hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Incident Escalated',{"options":{"color":"red"}} )
                        .then(function(data){
                        res.send(200);
                        });
                    }else{
                       hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Please create an incident using "OPEN", and join that room.',{"options":{"color":"gray"}} )
                        .then(function(data){
                        res.send(200);
                        });
                    }
                   

                }else if(slash === '/status' || (slash === "/incident" && qualifier === "STATUS")){
                  incident = incidentRooms[req.context.item.room.id];
                    if ( incident !== null ){
                       incident.updateStatus(incident.getLatestStatus().status,message?message:incident.getLatestStatus().message);
                        hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Incident Status Updated!  : ' + getLatestStatus(roomId).message)
                        .then(function(data){
                        res.send(200);
                        });
                      }else{
                       hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Please create an incident using "OPEN", and join that room.',{"options":{"color":"gray"}} )
                        .then(function(data){
                        res.send(200);
                        });
                    }

                }else if(slash === '/resolve' || (slash === "/incident" && qualifier === "RESOLVED")){
					incident = incidentRooms[req.context.item.room.id];
                    if ( incident !== null ){
                       incident.updateStatus("RESOLVED",message?message:getLatestStatus(roomId).message);
                  
                        hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Great Work Team - Please conduct RootCause.',{"options":{"color":"green"}})
                        .then(function(data){
                          res.send(200);
                        });
                    }else{
                       hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Please create an incident using "OPEN", and join that room.',{"options":{"color":"gray"}} )
                        .then(function(data){
                        res.send(200);
                        });
                    }
                }

            }else {
                //does not match pattern, and is not help..
                    
                hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'uhhh, not sure what to do with that..\n Type "/incident help" for assitance',{"options":{"color":"gray"}})
                    .then(function(data){
                      res.send(200);
                    });
                    
                   
            }

          

      },

      helpText: function(req,res){
          hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Usage: /incident (JIRA-123) (OPEN|ESCALATE|ACK|RESOLVED|STATUS)',{"options":{"color":"gray"}})
                          .then(function(data){
                            res.send(200);
                          });
      },
   

        notifyUser: function(req,res){
            if(incidentRooms[req.context.item.room.id]){
                hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'Thanks for joining  @' + req.context.item.sender.mention_name +' - There is an ongoing incident: "' + getLatestStatusText(req.context.item.room.id) +'", type /incident for more info',{"options":{"color":"gray"}})
                  .then(function(data){
                    res.send(200);
                  });            
            }
        },

        page: function(req, res){

            var messageText = req.context.item.message.message;
            var roomId = req.context.item.room.id;
            var arrMatches = messageText.match(pagePattern);
            console.log(arrMatches);
            hipchat.addMember(req.clientInfo,roomId,'@'+arrMatches[1]).then(function(data){
                hipchat.sendMessage(req.clientInfo, req.context.item.room.id, 'I have added @' + arrMatches[1] + ' to this room. Please join us.',{"options":{"color":"gray","format":"text"}})
                  .then(function(data){
                    res.send(200);
                  }); 
                });
        },
		purgeRooms:function(req,res){
			
          console.log("KILLING ALL ROOMS");
			var messageText = req.context.item.message.message;
            var roomId = req.context.item.room.id;
            var arrMatches = messageText.match(/^\/hurtme (.*)/);
			if( (arrMatches) && (1 in arrMatches)){
				patternToPurge = new RegExp(arrMatches[1]);
			}else{
				patternToPurge = new RegExp(/[A-Z]*-[0-9]*/);
			}
		
           hipchat.annilate(req.clientInfo,patternToPurge);
		}
    };//end return
};