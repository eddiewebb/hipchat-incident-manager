Want more, go [here](https://eddiewebb.atlassian.net/wiki/display/SHIT/Incident+Manager+for+HipChat+Home), or there rather.




# Incident Manager for HipChat

This plugin is built on the HipChat Connect Express web application framework. It is (an attempt at) a node.js application.
[Read the docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies).


# What it do

In a sentence? This plugin manages incidents by creating dedicated rooms and status tracking/reporting.  Additional integrations (JIRA, ZenDesk, etc) are possible.
![Overview Screenshot](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/c0176a2136d3c06126038a5ee7f02e9630d298d7/images/overview.png)

## Create incidents as new private rooms
Incident is opened up and begings tracking.

`/incident OPEN TIS-123 travelers reporting slowness in space transport tubes`

`/new TIS-123 travelers reporting slowness in space transport tubes`
![New Incidents](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/create_new.png)

![Welcomes all users](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/join_new_room.png)

### Create incidents in existing room.
THere is a 1 incident per room limit to prevent cross chatter, but you may use an existing room if not allocated.
`/incident OPENHERE TIS-123 travelers reportering slowness in space transport tubes`

## Track, escalate and acknowledge incidents
Once an incident is active updates can be added, or the incident can be escalated.

`/status I am just adding the latests update`

`/escalate @TaskOwner - we need you to bleep the blorpers`
![New Incidents](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/escalate.png)

`/acknowledge Bleeping now...`
![New Incidents](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/acknowledge.png)

`/resolve All systems are back online`
![New Incidents](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/resolve.png)

## Full incident reports
Private rooms are great for full history, but typing /incident in a room will print the incident report, with full history.
`/incident`

### Open
![Open Incident Report](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/report_open.png)
### Escalated
![Escalated Incident Report](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/report_escalated.png)
### Acknowledged 
![Acknowledged Incident Report](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/report_ack.png)
### Resolved
![Resolved Incident Report](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/report_resolved.png)

## Paging
You can add users when creating or in an incident. This will add permission and notify them.
`/page UserNotHere`
![Paging Out Users](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/25ee98d0dc467be1fc9a6f95f7ef01e7bb3e76f0/images/page_user.png)

Users will recieve standard HipChat notifications (pop-ups, email, etc)
![Standard notifications](https://bitbucket.org/eddiewebb/hipchat-incident-manager/raw/35b755a6bd277b4503a890155700f0a4280c81a4/images/email.png)

## Contributing

Head over to the wiki's getting started guide.
https://eddiewebb.atlassian.net/wiki/display/SHIT/Get+Started+with+Development

## TODOs
[https://eddiewebb.atlassian.net/issues/?filter=11100](https://eddiewebb.atlassian.net/issues/?filter=11100)


# Credits
Please see [Credits](https://bitbucket.org/eddiewebb/hipchat-incident-manager/src/3ad534cf79e90c551997076fe13e6d655d857abd/credits.md)